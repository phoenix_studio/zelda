package zelda.karacter;

/**
 * These are the directions link can face.
 *
 * @author KenYam Phoenix
 */
public enum Direction
{
    LEFT, RIGHT, UP, DOWN
}