package zelda.engine;

import java.awt.Graphics2D;

/**
 * Can the View draw this?
 *
 * @author KenYam Phoenix
 */
public interface DrawAble
{
    public void draw(Graphics2D g);
}